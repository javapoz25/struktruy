package com.sda.javapoz.struct;

import com.sda.javapoz.struct.stack.Nalesnik;
import com.sda.javapoz.struct.stack.Talerz;

import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Main {
    public static void main(String[] args) {
        // LIFO - Last in first out - struktura w której obiekty które były dodane ostatnie będą wyciągnięte pierwsze - STOS
        Talerz talerz = new Talerz();
        talerz.dodaj(new Nalesnik()); // 0
        talerz.dodaj(new Nalesnik()); // 1
        talerz.dodaj(new Nalesnik()); // 2
        talerz.dodaj(new Nalesnik()); // 3

        talerz.sprawdzCoJestNaSzczycie();
        System.out.println(talerz.zdejmij());
        System.out.println(talerz.zdejmij());
        System.out.println(talerz.zdejmij());
        System.out.println(talerz.zdejmij());

        // FIFO - First in first out - struktura w której pierwsze dodane będą pierwszymi wyjętymi - KOLEJKA
        // Klasa Queue -
        Queue<Integer> queueLista = new LinkedList<>();

        // Klasa Deque - double ended Queue (podwójna kolejka)
        Deque<Integer> dequeLista = new LinkedList<>();
        // wyciąganie elementów z kolejki odbywa się przy pomocy metody poll
        dequeLista.poll();

        // Klasa PriorityQueue - kolejka priorytetowa to struktura sortująca elementy w zależności od ich wagi.
        // - kobieta w ciąży
        // - starsze osoby
        // - wszyscy pozostali

    }
}
