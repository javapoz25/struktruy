package com.sda.javapoz.struct.exceptions;

public class Main {
    public static void main(String[] args) {

        metodaRekurencyjna(20000);

//        try{
//            metodaKtoraRzuciExc();
//            // jeśli exception jest typu Exception (nie runtime)
//            // to musimy mieć miejsce jego prawdopodobnego wystąpienia
//        }catch (ExampleException ee){
//            System.err.println("Wyjątek.");
//        }
    }

    private static void metodaRekurencyjna(int ilePojscWGlab) {
        if(ilePojscWGlab == 0){
            System.out.println("Doszedłem do końca rekurencji");
            return;
        }
        // każda metoda rekurencyjna powinna mieć warunek zakończenia
        metodaRekurencyjna(ilePojscWGlab-1);
    }

    // jeśli zgłaszamy coś wyjątkiem typu Exception - to wymuszamy jego obsługę
    // otwieranie pliku jest bardzo ryzykowne - ?
    //          - może go tam nie być
    //          - możemy nie mieć uprawnień
    //          - ścieżka pomylona
    //
    // Z tego względu np. otwieranie pliku rzuca Exception - żeby zwrócić uwagę na potencjalne błędy.

    public static void metodaKtoraRzuciExc() {
        throw new ExampleException();
    }
}
