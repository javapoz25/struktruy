package com.sda.javapoz.struct.watki;

public class Main {
    public static void main(String[] args) {

        // startowanie wątku który po zakończeniu swojej pracy jest zabijany (ten wątek ma jedno zadanie do wykonania).
        //      zadanie do wątku przekazujemy w konstruktorze;
        Thread wątek = new Thread(new Runnable() {
            private int licznik = 5000;

            @Override
            public void run() {
                // instrukcje do wykonywania przez wątek
                while (licznik > 0) {
                    licznik--;
                    System.out.println("Liczę w dół: " + licznik);
                    try {
                        Thread.sleep(1L);               // daje gigantyczną oszczędność na pracy procesora
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                System.out.println("Zakończyłem pracę");
            }
        });

        wątek.start();
        while (wątek.isAlive()){
            System.out.println("Czekam na drugi wątek.");
            try {
                Thread.sleep(1L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
