package com.sda.javapoz.struct.stream.filter;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            list.add(i);
        }

        // kolekcja jest typu Integer
//        List<Integer> wynik =list.stream() // sekwencyjne przetwarzanie kolejno elementów zbioru
//                .filter(new Predicate<Integer>() {
//                    @Override
//                    public boolean test(Integer integerZm) { // jeśli wynik metody jest true, to obiekt przepuszczany jest dalej
//                        return integerZm > 35;
//                    }
//                })
//                .collect(Collectors.toList());

//        List<Integer> wynik = list.stream() // sekwencyjne przetwarzanie kolejno elementów zbioru
//                .filter(integerZm -> { return integerZm > 35; })
//                .collect(Collectors.toList());

        List<Integer> wynik = list.stream() // sekwencyjne przetwarzanie kolejno elementów zbioru
                .filter(integer -> integer > 95)
                .collect(Collectors.toList());

        System.out.println(wynik);
    }
}
