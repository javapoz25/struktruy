package com.sda.javapoz.struct.stream.filter;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Student {
    private String name;
    private int age;
    private List<Double> grades;
}
