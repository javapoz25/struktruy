package com.sda.javapoz.struct.stream.filter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MainObiektowy {
    public static void main(String[] args) {
        List<Student> list = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            list.add(new Student("A-" + i, i, new ArrayList<>(Arrays.asList(3.0, 4.0))));
        }


        List<Student> wynik = list.stream() // sekwencyjne przetwarzanie kolejno elementów zbioru
//        list.stream() // sekwencyjne przetwarzanie kolejno elementów zbioru
                .filter(student -> student.getAge() > 95)
                .filter(student -> student.getAge() < 98)
                .collect(Collectors.toList()); // element kończący
//                .forEach(System.out::println);   // element kończący

        System.out.println(wynik);


        // Wyciągnięcie z obiektu Student (o wieku 96) jego ocen.
        // Inicjalnie, zaczynam od streamu studentów który chciałbym zamienić na stream ocen
//        List<List<Double>> wynikOceny = list.stream() // sekwencyjne przetwarzanie kolejno elementów zbioru
//                .filter(student -> student.getAge() == 96)
//                .map(new Function<Student, List<Double>>() {
//                    @Override
//                    public List<Double> apply(Student student) {
//                        return student.getGrades();
//                    }
//                })
//                .collect(Collectors.toList()); // element kończący

        List<List<Double>> wynikOceny = list.stream() // sekwencyjne przetwarzanie kolejno elementów zbioru
                .filter(student -> student.getAge() == 96)
                .map(student -> {return student.getGrades();})
                .collect(Collectors.toList()); // element kończący

        System.out.println(wynik);
    }
}
