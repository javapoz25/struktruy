package com.sda.javapoz.struct.stream;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 1.Napisz klasę Person, która ma pola: firstName, lastName, age oraz isMale.
 * Mając listę osób i korzystając ze streamów:
 * a) uzyskaj listę mężczyzn
 * b) uzyskaj listę dorosłych kobiet
 * c) uzyskaj Optional<Person> z dorosłym Jackiem
 * d) uzyskaj listę wszystkich nazwisk osób, które są w przedziale wiekowym: 15-19
 * e)* uzyskaj sumę wieku wszystkich osób
 * f)* uzyskaj średnią wieku wszystkich mężczyzn
 * g)** znajdź nastarszą osobę w liście
 */
public class Main {
    public static void main(String[] args) {
        Person person1 = new Person("Jacek", "Kowalski", 18, true);
        Person person2 = new Person("Jacek", "Górski", 15, true);
        Person person3 = new Person("Andżelika", "Dżoli", 25, false);
        Person person4 = new Person("Wanda", "Ibanda", 12, false);
        Person person5 = new Person("Marek", "Marecki", 17, true);
        Person person6 = new Person("Johny", "Brawo", 25, true);
        Person person7 = new Person("Stary", "Pan", 80, true);
        Person person8 = new Person("Newbie", "Noob", 12, true);
        Person person9 = new Person("Newbies", "Sister", 19, false);

        List<Person> people = new ArrayList<>(Arrays.asList(
                person1,
                person2,
                person3,
                person4,
                person5,
                person6,
                person7,
                person8,
                person9
        ));


//         * a) uzyskaj listę mężczyzn
        List<Person> a_mezczyzni = people.stream()
                .filter(person -> person.isMale())
//                .filter(Person::isMale)
                .collect(Collectors.toList());

//         * b) uzyskaj listę dorosłych kobiet
        List<Person> b_dorosle_kobiety = people.stream()
                .filter(person -> !person.isMale())
                .filter(person -> person.getAge() >= 18)
                .collect(Collectors.toList());

//         * c) uzyskaj Optional<Person> z dorosłym Jackiem
        // Poniżej szukanie wszystkich dorosłych Jacków
//        List<Person> c_dorosly_jacek = people.stream()
//                .filter(person -> person.getAge() >= 18)
//                .filter(person -> person.getFirstName().equalsIgnoreCase("jacek"))
//                .collect(Collectors.toList());
        Optional<Person> c_dorosly_jacek = people.stream()
                .filter(person -> person.getAge() >= 18)
                .filter(person -> person.getFirstName().equalsIgnoreCase("jacek"))
                .findFirst();
        if(c_dorosly_jacek.isPresent()){
            Person person = c_dorosly_jacek.get();
            System.out.println("Znalezlismy doroslego Jacka: " + person);
        }


//         * d) uzyskaj listę wszystkich nazwisk osób, które są w przedziale wiekowym: 15-19
        List<String> d_nazwiska_15_19 = people.stream()
                .filter(person -> person.getAge() >= 15 && person.getAge() <=19)
//                .map(Person::getLastName)
                .map(person -> person.getLastName())
                .collect(Collectors.toList());

//         * e)* uzyskaj sumę wieku wszystkich osób
        Integer e_suma_wiekow = people.stream()
//                .map(person -> person.getAge()) //map jest funkcją która mapuje na inny typ obiektowy (nie musi to być typ numeryczny)
                .mapToInt(person -> person.getAge())
                .sum();

//         * f)* uzyskaj średnią wieku wszystkich mężczyzn
        OptionalDouble f_srednia_wiekow = people.stream()
                .filter(person -> person.isMale())
                .mapToInt(person -> person.getAge())
                .average();
        // ponieważ wartość zwrócona może nie wystąpić, to typ zwracany to Optional<Double>

//         * g)** znajdź nastarszą osobę w liście
        OptionalInt wynikMax = people.stream()
                .mapToInt(person -> person.getAge())
                .max();

        if(wynikMax.isPresent()){
            // wiek osoby najstarszej
            Integer maxWiek = wynikMax.getAsInt();

            Optional<Person> g_najstarsza = people.stream()
                    .filter(person -> person.getAge() == maxWiek)
                    .findAny();
            // znaleziona osoba najstarsza: g_najstarsza
        }


        Optional<Person> g_najstarsza = people.stream()
                .filter(person -> {
                    OptionalInt wynik = people.stream()
                            .mapToInt(per -> per.getAge())
                            .max();
                    return person.getAge() == wynik.getAsInt();
                })
                .findAny();

    }
}
