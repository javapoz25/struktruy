package com.sda.javapoz.struct.stream;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

/**
 * 2. Stwórz klasę Programmer, która ma klasę Person jako pole. Poza tym, posiada listę języków, którymi się posługuje. Mogą być one reprezentowane przez klasę String.
 * Mając listę programistów i korzystając ze streamów:
 * a) uzyskaj listę programistów, którzy są mężczyznami
 * b) uzyskaj listę niepełnoletnich programistów (obydwóch płci), którzy piszą w Cobolu
 * c) uzyskaj listę programistów, którzy znają więcej, niż jeden język programowania
 * d) uzyskaj listę programistek, które piszą w Javie i Cpp
 * e) uzyskaj listę męskich imion
 * f) uzyskaj set wszystkich języków opanowanych przez programistów
 * g) uzyskaj listę nazwisk programistów, którzy znają więcej, niż dwa języki
 * h) sprawdź, czy istnieje chociaż jedna osoba, która nie zna żadnego języka
 * i)* uzyskaj ilość wszystkich języków opanowanych przez programistki
 * j) uzyskaj średnią ilość opanowanych języków przez programistki
 * k) uzyskaj płeć programisty, który zna największą ilość języków
 * l) zwroc listę językow posortowanych wedlug powszechnosci
 */
public class Main2 {
    public static void main(String[] args) {
        Person person1 = new Person("Jacek", "Kowalski", 18, true);
        Person person2 = new Person("Jacek", "Górski", 15, true);
        Person person3 = new Person("Andżelika", "Dżoli", 25, false);
        Person person4 = new Person("Wanda", "Ibanda", 12, false);
        Person person5 = new Person("Marek", "Marecki", 17, true);
        Person person6 = new Person("Johny", "Brawo", 25, true);
        Person person7 = new Person("Stary", "Pan", 80, true);
        Person person8 = new Person("Newbie", "Noob", 12, true);
        Person person9 = new Person("Newbies", "Sister", 19, false);
        List<String> languages1 = Arrays.asList("Java;Cobol;Cpp;Lisp".split(";"));
        List<String> languages2 = Arrays.asList("Java;Lisp".split(";"));
        List<String> languages3 = Arrays.asList("Java;Cobol;Cpp;Lisp;C#".split(";"));
        List<String> languages4 = Arrays.asList("C#;C;Cpp".split(";"));
        List<String> languages5 = Arrays.asList("Java;Assembler;Scala;Cobol".split(";"));
        List<String> languages6 = Arrays.asList("Java;Scala".split(";"));
        List<String> languages7 = Arrays.asList("C#;C".split(";"));
        List<String> languages8 = Collections.emptyList();
        List<String> languages9 = Arrays.asList("Java");

        Programmer programmer1 = new Programmer(person1, languages1);
        Programmer programmer2 = new Programmer(person2, languages2);
        Programmer programmer3 = new Programmer(person3, languages3);
        Programmer programmer4 = new Programmer(person4, languages4);
        Programmer programmer5 = new Programmer(person5, languages5);
        Programmer programmer6 = new Programmer(person6, languages6);
        Programmer programmer7 = new Programmer(person7, languages7);
        Programmer programmer8 = new Programmer(person8, languages8);
        Programmer programmer9 = new Programmer(person9, languages9);

        List<Programmer> programmers = Arrays.asList(programmer1, programmer2, programmer3, programmer4,
                programmer5, programmer6, programmer7, programmer8, programmer9);

        System.out.println(programmers);

// * a) uzyskaj listę programistów, którzy są mężczyznami
        List<Programmer> a_mezczyzni = programmers.stream()
                .filter(programmer -> programmer.getPerson().isMale())
                .collect(Collectors.toList());

// * b) uzyskaj listę niepełnoletnich programistów (obydwóch płci), którzy piszą w Cobolu
        List<Programmer> list_niepelnoletni_cobolowcy = programmers.stream()
                .filter(programmer -> programmer.getPerson().getAge() < 18)
                .filter(programmer -> programmer.getLanguages().contains("Cobol"))
                .collect(Collectors.toList());

// * c) uzyskaj listę programistów, którzy znają więcej, niż jeden język programowania
        List<Programmer> kumaci_programisci = programmers.stream()
                .filter(programmer -> programmer.getLanguages().size() > 1)
                .collect(Collectors.toList());

// * d) uzyskaj listę programistek, które piszą w Javie i Cpp
        List<Programmer> programistkiJavaCPP = programmers.stream()
                .filter(programmer -> !programmer.getPerson().isMale())
                .filter(programmer -> programmer.getLanguages().contains("Java"))
                .filter(programmer -> programmer.getLanguages().contains("Cpp"))
                .collect(Collectors.toList());

// * e) uzyskaj listę męskich imion
        Set<String> imionaMeskie = programmers.stream()
                .filter(programmer -> programmer.getPerson().isMale())
                .map(programmer -> programmer.getPerson().getFirstName())
                .collect(Collectors.toSet());

// * f) uzyskaj set wszystkich języków opanowanych przez programistów
        Set<String> f_jezykiProg = programmers.stream()
                .flatMap(programmer -> programmer.getLanguages().stream()) //stream < List <String>>
                .collect(Collectors.toSet());
        System.out.println(f_jezykiProg);

// * g) uzyskaj listę nazwisk programistów, którzy znają więcej, niż dwa języki
        List<String> nazwiska_super_kumatych = programmers.stream()
                .filter(programmer -> programmer.getLanguages().size() > 2)
                .map(programmer -> programmer.getPerson().getLastName())
                .collect(Collectors.toList());

// * h) sprawdź, czy istnieje chociaż jedna osoba, która nie zna żadnego języka
        Optional<Programmer> programmerOptional = programmers.stream()
                .filter(programmer -> programmer.getLanguages().size() == 0)
                .findAny();

// * i)* uzyskaj ilość wszystkich języków opanowanych przez programistki
        Long ilosc_jezykow_opanowanych_przez_programistki = programmers.stream()
                .filter(programmer -> !programmer.getPerson().isMale())  // wszystkie programistki
                .flatMap(programmer -> programmer.getLanguages().stream()) // wszystkie języki opanowane przez wszystkie programistki
                .distinct()                                                 // usuwa ze streamu duplikaty
                .count();

// * j) uzyskaj średnią ilość opanowanych języków przez programistki
        OptionalDouble sredniaIloscJezykow = programmers.stream()
                .filter(programmer -> !programmer.getPerson().isMale())
                .mapToDouble(programmer -> programmer.getLanguages().size())
                .average();

// * k) uzyskaj płeć programisty, który zna największą ilość języków
        OptionalInt optionalInt = programmers.stream()
                .mapToInt(programmer -> programmer.getLanguages().size())
                .max();

        if (optionalInt.isPresent()) {
            Integer iloscJezykowKtoraOpanowalNajlepszyProgramista = optionalInt.getAsInt();

            Optional<Programmer> najkumatszyProgramista = programmers.stream()
                    .filter(programmer -> programmer.getLanguages().size() == iloscJezykowKtoraOpanowalNajlepszyProgramista)
                    .findFirst();
            //... itd. wyciągamy płeć
            if (najkumatszyProgramista.isPresent()) {
                Programmer programmer = najkumatszyProgramista.get();
                System.out.println("Płeć to: " + programmer.getPerson().isMale());
            }
        }

        Optional<Programmer> optionalProgrammer = programmers.stream()
                .max(new Comparator<Programmer>() {
                    @Override
                    public int compare(Programmer o1, Programmer o2) {
                        return Integer.compare(o1.getLanguages().size(), o2.getLanguages().size());
                    }
                });
        if (optionalProgrammer.isPresent()) {
            Programmer najkumatszy = optionalProgrammer.get();
            System.out.println("Płeć to: " + najkumatszy.getPerson().isMale());
        }

// * l) zwroc listę językow posortowanych wedlug powszechnosci
        // Wynik = Map<String, Long>

        // Cpp, Java, Cpp, Cobol, C
        // Cpp -> 2
        // Java -> 1
        Map<String, Long> zliczenie = programmers.stream()
                .flatMap(programmer -> programmer.getLanguages().stream())                      // wyciągamy języki programowania
                .collect(Collectors.toMap(
                        s -> s,                                 // co jest kluczem
                        s -> 1L,                                // jak zdefiniować wartość (co znajduje się w wartości - mapa)
                        new BinaryOperator<Long>() {            // co zrobić, jeśli drugi raz trafił się ten sam klucz.
                            @Override
                            public Long apply(Long aLong, Long aLong2) {
                                return aLong + aLong2;
                            }
                        }
                ))
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))
                .collect(
                        Collectors.toMap(
                                Map.Entry::getKey,          // dodaj do mapy, klucz to klucz
                                Map.Entry::getValue,        // dodaj do mapy, wartość to wartość
                                (oldValue, newValue) -> oldValue, LinkedHashMap::new));   // zbierz wszystkie wyniki do linked listy
                                // linked lista zachowuje kolejność wstawiania elementów

        System.out.println("ZLiczenia: " + zliczenie);


    }
}
