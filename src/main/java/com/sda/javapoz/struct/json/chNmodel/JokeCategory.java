package com.sda.javapoz.struct.json.chNmodel;

public enum JokeCategory {
    animal,
    career,
    celebrity,
    dev,
    explicit,
    fashion,
    food,
    history,
    money,
    movie,
    music,
    political,
    religion,
    science,
    sport,
    travel
}
