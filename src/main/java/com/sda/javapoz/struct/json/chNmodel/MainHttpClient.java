package com.sda.javapoz.struct.json.chNmodel;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class MainHttpClient {
    public static void main(String[] args) {
        // klient którym wyślę ramkę do serwisu
        HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();

        // Metoda HTTP - wykonuje zapytanie klientem, w zapytaniu definiujemy jaką czynność chcemy wykonać na serwisie:
        //      GET     - pobranie (get())
        //      POST    - modyfikacja (set() - ustaw na pozycję)
        //      PUT     - umieść (add())
        //      DELETE  - usuń (remove())
        //  CRUD - CREATE READ UPDATE DELETE

        // tworzę ramkę, którą wyślę do serwisu
        HttpRequest request = HttpRequest
                .newBuilder()
                .GET()
                .uri(URI.create("https://api.chucknorris.io/jokes/random?category=movie"))
                .build();

        // drugim parametrem metody send jest klasa która przetwarza treść odpowiedz
        for (int i = 0; i < 10; i++) {
            requestJokeFromChuckNorrisApi(httpClient, request);
        }

    }

    private static void requestJokeFromChuckNorrisApi(HttpClient httpClient, HttpRequest request) {
        try {
            HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString()); // spodziewamy się treści w postaci tekstu

            if (response.statusCode() >= 200 && response.statusCode() <= 299) {
                System.out.println("Response ok!");
                ObjectMapper mapper = new ObjectMapper();
                ChuckNorrisJoke responseJoke = mapper.readValue(response.body(), ChuckNorrisJoke.class);
                System.out.println("Dowcip o identyfikatorze: " + responseJoke.getId() + " tresc: " + responseJoke.getValue()); // treść dowcipu

            }else if(response.statusCode() >400 && response.statusCode() <=599){
                System.out.println("Error response code! " + response.statusCode());

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
