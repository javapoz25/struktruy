package com.sda.javapoz.struct.json;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

// POJO - Plain Old Java Object - oznacza klasę która spełnia następujące warunki:
//           - pusty konstruktor (bezparametrowy)
//           - prywatne pola
//           - gettery i settery do wszystkich pól
@Data
@NoArgsConstructor
@AllArgsConstructor // na moje potrzeby, nie jest wymagane przez POJO
public class Student {
    private String imie;
    private String nazwisko;
}

// format JSON:
/*
        {
            "imie": "Json",
            "nazwisko": "Statham"
        }
* */
// format XML:  SOAP
/*
        <student>
            <imie>Json</imie>
            <nazwisko czyRodowe="true">Statham</nazwisko>
        </student>
* */

