package com.sda.javapoz.struct.json.chNmodel;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
// snake case - notacja podkreślnikowa
// camel case

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ChuckNorrisJoke {
    //"categories": [ ],
    private List<JokeCategory> categories;

    //"icon_url": "https://assets.chucknorris.host/img/avatar/chuck-norris.png",
    private String iconUrl; // snake case = icon_url

    //"id": "lhw5nmosqkcbrdivtsjwgq",
    private String id;

    //"url": "https://api.chucknorris.io/jokes/lhw5nmosqkcbrdivtsjwgq",
    private String url;

    //"value": "Chuck Norris doesn't chew gum. Chuck Norris chews tin foil."
    private String value;

    //created_at
    private String createdAt;
    private String updatedAt;


//    private String cosWlasnego;
}
