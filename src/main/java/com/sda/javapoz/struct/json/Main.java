package com.sda.javapoz.struct.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Main {
    public static void main(String[] args) {
        ObjectMapper objectMapper = new ObjectMapper();

        Student student = new Student("Jan", "Kowalski");
        // zamieniam obiekt na tekst (JSON)
        String jsonValue = null;
        try {
            jsonValue = objectMapper.writeValueAsString(student);
            System.out.println("Obiekt w postaci JSON: \n" + jsonValue);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        // zamieniam tekst (z formatu JSON) na obiekt
        // drugim parametrem jest typ obiektu do którego chcemy zrzutować treść JSON.
        try {
            Student obiektZTekstu = objectMapper.readValue("{\"imie\":\"Janek\",\"nazwisko\":\"Nowak\"}", Student.class);
            System.out.println("Obiektowo: \n" + obiektZTekstu);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
