package com.sda.javapoz.struct.priorityqueue;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.time.LocalDateTime;

@ToString
@Getter
@Setter
public class Klient /*implements Comparable*/ {
    private String imie;
    private boolean wCiazy;
    private int wiek;                           // > 65 lat
    private LocalDateTime dolaczenieDoKolejki;

    public Klient(String imie, boolean wCiazy, int wiek) {
        this.imie = imie;
        this.wCiazy = wCiazy;
        this.wiek = wiek;
    }
}
