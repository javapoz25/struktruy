package com.sda.javapoz.struct.priorityqueue;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * // - kobieta w ciąży
 * // - starsze osoby
 * // - wszyscy pozostali
 */
public class Apteka {
    private PriorityQueue<Klient> kolejka = new PriorityQueue<>(new Comparator<Klient>() {
        // -1 oznacza że k1 powinien być wyżej.
        @Override
        public int compare(Klient k1, Klient k2) {
            // jeśli k1 ma pierwszeństwo, to zwrócona wartość = 1
            if (k1.isWCiazy() && !k2.isWCiazy()) {
                return -1; // pierwszeństwo dla osób w ciąży
            } else if (!k1.isWCiazy() && k2.isWCiazy()) {
                return 1;
            }

            if (k1.getWiek() > 65 && k2.getWiek() < 65) {
                return -1; // pierwszeństwo dla osób starszych
            }else if(k1.getWiek() < 65 && k2.getWiek() > 65){
                return 1;
            }

            return k1.getDolaczenieDoKolejki().compareTo(k2.getDolaczenieDoKolejki());
        }
    });

    public void dodaj(Klient k) {
        // moment wywołania metody ustawia czas dołączenia do kolejki
        k.setDolaczenieDoKolejki(LocalDateTime.now());
        kolejka.add(k);
    }

    public Klient wyciągnijZKolejki() {
        return kolejka.poll();
    }
}
