package com.sda.javapoz.struct.priorityqueue;

import com.sda.javapoz.struct.stack.Nalesnik;
import com.sda.javapoz.struct.stack.Talerz;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // FIFO - First in first out - struktura w której pierwsze dodane będą pierwszymi wyjętymi - KOLEJKA
        // Klasa Queue -
//        Queue<Integer> queueLista = new LinkedList<>();

        // Klasa Deque - double ended Queue (podwójna kolejka)
//        Deque<Integer> dequeLista = new LinkedList<>();
        // wyciąganie elementów z kolejki odbywa się przy pomocy metody poll
//        dequeLista.poll();

        // Klasa PriorityQueue - kolejka priorytetowa to struktura sortująca elementy w zależności od ich wagi.
        // - kobieta w ciąży
        // - starsze osoby
        // - wszyscy pozostali
        Scanner scanner = new Scanner(System.in);
        Apteka apteka = new Apteka();

        String komenda;

        do {
            System.out.println("Podaj komende:");
            komenda = scanner.nextLine();

            String[] slowa = komenda.split(" ");

            /*
dodaj Maria true 20
dodaj Basia false 35
dodaj Asia false 45
dodaj Zosia false 79
dodaj Mia true 79
dodaj Rafał true 60
dodaj Paweł false 90

             */
            if (slowa[0].equalsIgnoreCase("dodaj")) {
                String imie = slowa[1];
                boolean czyWCiazy = Boolean.parseBoolean(slowa[2]);
                int wiek = Integer.parseInt(slowa[3]);

                Klient k = new Klient(imie, czyWCiazy, wiek);
                apteka.dodaj(k);

            } else if (slowa[0].equalsIgnoreCase("wyciagnij")) {
                System.out.println("Wyciągam z kolejki: " + apteka.wyciągnijZKolejki());
            }


        } while (!komenda.equalsIgnoreCase("quit"));
    }
}
