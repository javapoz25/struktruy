package com.sda.javapoz.struct.stack;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class Main {
    public static void main(String[] args) {
        // LIFO - Last in first out - struktura w której obiekty które były dodane ostatnie będą wyciągnięte pierwsze - STOS
        Talerz talerz = new Talerz();
        talerz.dodaj(new Nalesnik()); // 0
        talerz.dodaj(new Nalesnik()); // 1
        talerz.dodaj(new Nalesnik()); // 2
        talerz.dodaj(new Nalesnik()); // 3

        talerz.sprawdzCoJestNaSzczycie();
        System.out.println(talerz.zdejmij());
        System.out.println(talerz.zdejmij());
        System.out.println(talerz.zdejmij());
        System.out.println(talerz.zdejmij());


    }
}
