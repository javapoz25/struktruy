package com.sda.javapoz.struct.stack;

import java.util.Optional;
import java.util.Stack;

public class Talerz {
    // LAST IN FIRST OUT - STOS
    private Stack<Nalesnik> nalesniki = new Stack<>();

    public void dodaj(Nalesnik nalesnik){
        // zamiast add (w listach i setach) używamy push
        System.out.println("Dodaję : " + nalesnik);
        nalesniki.push(nalesnik);
    }

    public Optional<Nalesnik> zdejmij(){
        if(nalesniki.empty()){
            return Optional.empty();
        }
        // metoda peek() - służy do podglądania co jest na stosie (ale nie zdejmuje elementu)
        // metoda pop() - służy do zdejmowania - po życiu zwraca element ze stosu
        return Optional.of(nalesniki.pop());
    }

    public void sprawdzCoJestNaSzczycie(){
        System.out.println("Na szczycie znajduje się: " + nalesniki.peek());
    }
}
