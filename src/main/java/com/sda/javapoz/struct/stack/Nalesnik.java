package com.sda.javapoz.struct.stack;

import lombok.ToString;

import java.time.LocalDateTime;

@ToString
public class Nalesnik {
    // pole statyczne, nie jest polem klasy
    // statyczny kicznik
    private static Long LICZNIK = 0L;

    private final Long numer;
    private final LocalDateTime dataStworzenia;

    public Nalesnik() {
        this.numer = LICZNIK++;
        this.dataStworzenia = LocalDateTime.now();
    }
}
